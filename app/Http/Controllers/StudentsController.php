<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class StudentsController extends Controller
{
   public function index(){
      $hasil = DB::table('mahasiswa')->get();

      return view('index',['data_mhs'=>$hasil]);
      
  }
   public function back(){
      return redirect ('/');
    }
    
   public function create(){
      return view('create');
    }

   public function insert(Request $req){
      $nama_mahasiswa = $req->nama_mahasiswa;
      $nim_mahasiswa = $req->nim_mahasiswa;
      $kelas_mahasiswa = $req->kelas_mahasiswa;
      $prodi_mahasiswa = $req->prodi_mahasiswa;
      $fakultas_mahasiswa = $req->fakultas_mahasiswa;
      
      DB::table('mahasiswa')->insert(
          ['nama_mahasiswa'=>$nama_mahasiswa,
          'nim_mahasiswa'=>$nim_mahasiswa,
          'kelas_mahasiswa'=>$kelas_mahasiswa,
          'prodi_mahasiswa'=>$prodi_mahasiswa,
          'fakultas_mahasiswa'=>$fakultas_mahasiswa]
      );
      return redirect('/');
  }

    public function deleteData($id){
      DB::table('mahasiswa')->where('id', $id)->delete();
      return redirect('/');
  }

    public function edit($id){
      $editData = DB::table('mahasiswa')->where('id',$id)->first();
      return view('edit',compact('editData'));
  }

  public function update(Request $req){
   DB::table('mahasiswa')->where('id',$req['id'])->update(
       ['nama_mahasiswa'=>$req['nama_mahasiswa'],
       'nim_mahasiswa'=>$req['nim_mahasiswa'],
       'kelas_mahasiswa'=>$req['kls_mahasiswa'],
       'prodi_mahasiswa'=>$req['prodi_mahasiswa'],
       'fakultas_mahasiswa'=>$req['fakultas_mahasiswa'],
       
       ]
   );
   return redirect('/');
  }  

    public function read($id){
     $data=mahasiswa::find($id);
     return view('read',compact('data'));
    }
}