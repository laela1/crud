@extends('layout.master')

@section('content')
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>Data Mahasiswa</h4>
            </div>
            <div class="panel-body">
                    <div class="form-group row">
                        <label for="nim_mahasiswa" class="col-sm-2 col-form-label">NIM</label>
                        <div class="col-sm-10">
                            <input type="text" name="nim_mahasiswa" id="nim_mahasiswa" value="{{$data->nim_mahasiswa}}" class="form-control" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nama_mahasiswa" class="col-sm-2 col-form-label">Nama Lengkap</label>
                        <div class="col-sm-10">
                            <input type="text" name="nama_mahasiswa" id="nama_mahasiswa" value="{{$data->nama_mahasiswa}}" class="form-control" readonly>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="kelas_mahasiswa" class="col-sm-2 col-form-label">Kelas</label>
                        <div class="col-sm-10">
                            <input type="text" name="kelas_mahasiswa" id="kelas_mahasiswa" value="{{$data->kelas_mahasiswa}}" class="form-control" readonly>
                        </div>
                    </div>                    
                    <div class="form-group row">    
                        <label for="prodi_mahasiswa" class="col-sm-2 col-form-label">Program Studi</label> 
                        <div class="col-sm-10">   
                            <input type="text" name="prodi_mahasiswa" id="prodi_mahasiswa" value="{{$data->prodi_mahasiswa}}" class="form-control" readonly>
                        </div>
                    </div>
                    <div class="form-group row">    
                        <label for="fakultas_mahasiswa" class="col-sm-2 col-form-label">Fakultas</label> 
                        <div class="col-sm-10">   
                            <input type="text" name="fakultas_mahasiswa" id="fakultas_mahasiswa" value="{{$data->fakultas_mahasiswa}}" class="form-control" readonly>
                        </div>
                    </div>
                    <form action="{{url('back')}}" method="get">
                        <div class="form-group">
                            <input type="submit" value="Kembali" class="btn btn-success">
                        </div>
                    </form>
            </div>
        </div>
@endsection