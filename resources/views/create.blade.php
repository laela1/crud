@extends('layout.master')

@section('content')
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>Tambah Data Mahasiswa</h4>
            </div>
            <div class="panel-body">
                <form action="{{url('insert')}}" method="post">
                    <div class="form-group row">
                        <label for="nim_mahasiswa" class="col-sm-2 col-form-label">NIM</label>
                        <div class="col-sm-10">
                            <input type="text" name="nim_mahasiswa" id="nim_mahasiswa" class="form-control" required="require">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="nama_mahasiswa" class="col-sm-2 col-form-label">Nama Lengkap</label>
                        <div class="col-sm-10">
                            <input type="text" name="nama_mahasiswa" id="nama_mahasiswa" class="form-control">
                        </div>
                    </div> 
                    <div class="form-group row">
                        <label for="kelas_mahasiswa" class="col-sm-2 col-form-label">Kelas</label>
                        <div class="col-sm-10">
                            <input type="text" name="kelas_mahasiswa" id="kelas_mahasiswa" class="form-control">
                        </div>
                    </div> 
                    <div class="form-group row">
                        <label for="prodi_mahasiswa" class="col-sm-2 col-form-label">Program Studi</label>
                        <div class="col-sm-10">
                            <input type="text" name="prodi_mahasiswa" id="prodi_mahasiswa" class="form-control">
                        </div>
                    </div>                    
                    <div class="form-group row">
                        <label for="fakultas_mahasiswa" class="col-sm-2 col-form-label">Fakultas</label>
                        <div class="col-sm-10">
                            <input type="text" name="fakultas_mahasiswa" id="fakultas_mahasiswa" class="form-control">
                        </div>
                    </div>  
                    <div class="form-group">
                        <input type="submit" name="send" id="send" value="Simpan" class="btn btn-success">{!!csrf_field()!!}                       
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection